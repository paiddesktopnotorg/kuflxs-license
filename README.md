# The kuflxs license

Every free software project needs a license. This license strives to provide a license that has full protection of the author's work, and aims to minimise the amount of legal loopholes possible in many currently existing copyleft licenses and prevents usage of software in a permissive manner allowing companies to disregard the work of the author.

# Legal disclaimer

While the kuflxs license aims to provide a usable copyleft license, this license is currently not holding any legal grounds in any manner whatsoever and in any region, territory or country. We advise against using the license until enough time has passed and it has matured enough.